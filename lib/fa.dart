import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}
void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{
  static ThemeData appThemeLinght(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
          color: Colors.blue.shade100,
          iconTheme: IconThemeData(
            color: Colors.white,
          )
      ),
      iconTheme:  IconThemeData(
          color: Colors.grey.shade600
      ),
    );
  }
  static ThemeData appThemeDrak(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
          color: Colors.grey,
          iconTheme: IconThemeData(
            color: Colors.white,
          )
      ),
      iconTheme:  IconThemeData(
          color: Colors.white
      ),
    );
  }
}


class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme =APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLinght()
          : MyAppTheme.appThemeDrak(),
      home: Scaffold(
        appBar: builAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child : Icon(Icons.threesixty),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}
Widget buildSunnyButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.wb_sunny,
          color: Colors.orangeAccent,
        ),
        onPressed: () {},
      ),
      Text("30%"),
    ],
  );
}
Widget buildCloudButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.blueAccent,
        ),
        onPressed: () {},
      ),
      Text("50%"),
    ],
  );
}
Widget buildBeachButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.beach_access,
          color: Colors.indigo.shade900,
        ),
        onPressed: () {},
      ),
      Text("18%"),
    ],
  );
}
Widget buildAcButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.ac_unit,
          color: Colors.blue.shade400,
        ),
        onPressed: () {},
      ),
      Text("2%"),
    ],
  );
}

Widget CListTile(){
  return ListTile(
    leading: Icon(Icons.brightness_7_outlined),
    title: Text("รู้สึกเหมือน"),
    subtitle: Text("29 cํ"),

  );
}
Widget EListTile(){
  return ListTile(
    leading: Icon(Icons.remove_red_eye_outlined),
    title: Text("ทัศนวิสัย"),
    subtitle: Text("11 กม."),
  );
}

Widget UVListTile() {
  return ListTile(
    leading: Icon(Icons.security),
    title: Text("UV"),
    subtitle: Text("ปานกลาง"),
  );
}
Widget snowingListTile() {
  return ListTile(
    leading: Icon(Icons.cloudy_snowing),
    title: Text("ความชื้น"),
    subtitle: Text("47%"),
  );
}

Widget TodayTile() {
  return ListTile(
    leading: Text("   Today"),
    title: Text("              28 cํ                            36 cํ"),
    trailing: IconButton(
      icon: Icon(Icons.wb_cloudy_outlined),
      color: Colors.blue,
      onPressed: (){},
    ),
  );
}
Widget SatTile() {
  return ListTile(
    leading: Text("     Sat"),
    title: Text("               27 cํ                             35 cํ"),
    trailing: IconButton(
      icon: Icon(Icons.wb_cloudy_outlined),
      color: Colors.blue,
      onPressed: (){},
    ),
  );
}
Widget SunTile() {
  return ListTile(
    leading: Text("     Sun"),
    title: Text("               27 cํ                             35 cํ"),
    trailing: IconButton(
      icon: Icon(Icons.wb_cloudy_outlined),
      color: Colors.blue,
      onPressed: (){},
    ),
  );
}
Widget MonTile() {
  return ListTile(
    leading: Text("     Mon"),
    title: Text("               30 cํ                             39 cํ"),
    trailing: IconButton(
      icon: Icon(Icons.wb_sunny_outlined),
      color: Colors.red,
      onPressed: (){},
    ),
  );
}
Widget TueTile() {
  return ListTile(
    leading: Text("     Tue"),
    title: Text("               29 cํ                             32 cํ"),
    trailing: IconButton(
      icon: Icon(Icons.cloudy_snowing),
      color: Colors.grey.shade600,
      onPressed: (){},
    ),
  );
}
Widget WedTile() {
  return ListTile(
    leading: Text("     Wed"),
    title: Text("               27 cํ                             32 cํ"),
    trailing: IconButton(
      icon: Icon(Icons.cloudy_snowing),
      color: Colors.grey.shade600,
      onPressed: (){},
    ),
  );
}
Widget ThurTile() {
  return ListTile(
    leading: Text("     Thur"),
    title: Text("               30 cํ                             36 cํ"),
    trailing: IconButton(
      icon: Icon(Icons.wb_sunny_outlined),
      color: Colors.red,
      onPressed: (){},
    ),
  );
}

AppBar builAppBarWidget(){
  return AppBar(
    backgroundColor: Colors.blue,

    title: Text("Bang Saen",
      style: TextStyle(fontSize: 25),
    ),



    leading: Icon(
        Icons.location_on,
        color: Colors.red
    ),

  );
}
ListView buildBodyWidget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[


          Container(

            child: Stack(
              children: [
                Image.network(
                  "https://i02.appmifile.com/images/2019/10/03/043b82a3-d0e4-43ce-bb92-430adac69172.jpg",
                  fit: BoxFit.cover,
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.all(20),

                  child: Text("28 cํ ",
                    style: TextStyle(fontSize: 70, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),

          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:EdgeInsets.all(6.0),
                  child:Text("วันนี้",
                    style: TextStyle(fontSize: 25),
                  ),
                ),
              ],

            ),
          ),
          Divider(
            // color: Colors.black,
          ),
          Container(
            margin: EdgeInsets.only(top: 8,bottom: 8),
            child: Theme(
              data:ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.black,
                  )
              ),
              child: profileActionItems(),
            ),
          ),

          Divider(
            color: Colors.grey,
          ),
          TodayTile(),
          SatTile(),
          SunTile(),
          MonTile(),
          TueTile(),
          WedTile(),
          ThurTile(),
          Divider(
            color: Colors.grey,
          ),
          CListTile(),
          EListTile(),
          UVListTile(),
          snowingListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      buildSunnyButton(),
      buildCloudButton(),
      buildBeachButton(),
      buildAcButton(),

    ],
  );
}